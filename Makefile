APP_CONTAINER=docker-compose exec -T php sh -c

dockerize:
	docker-compose stop
	docker network create --driver bridge lambda-test || true
	docker-compose up -d
	$(APP_CONTAINER) "composer install --no-interaction;"

fix-permisions:
	sudo chown -R $$USER:$$USER .