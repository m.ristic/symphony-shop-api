<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;


class HomeController
{
    public function index()
    {
        return new JsonResponse(
            ['test' => 1]
        );
    }
}
